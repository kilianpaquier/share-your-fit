package com.example.shareyourfit.data.model.user;

public enum Role {
    COACH("Coach"),
    ADEPT("Adepte");

    private String role;

    Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
