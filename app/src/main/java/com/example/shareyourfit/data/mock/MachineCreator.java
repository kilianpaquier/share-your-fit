package com.example.shareyourfit.data.mock;

import androidx.annotation.NonNull;

import com.example.shareyourfit.data.model.machine.Machine;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.data.utils.QRCodeScan;
import com.example.shareyourfit.ui.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;

import java.util.List;
import java.util.Objects;

public class MachineCreator {

    private FirestoreRepository<Machine> machineRepository;

    public MachineCreator() {
        machineRepository = new FirestoreRepository<>(Machine.class, MainActivity.MACHINE_REPO);

        machineRepository.getAll().addOnCompleteListener(new OnCompleteListener<List<Machine>>() {
            @Override
            public void onComplete(@NonNull Task<List<Machine>> task) {
                if (task.isSuccessful()) {
                    for (Machine machine: Objects.requireNonNull(task.getResult())) {
                        QRCodeScan.createQRCode(machine);
                    }
                }
            }
        });
    }

    public void createMachines() {
        DocumentReference reference = machineRepository.getCollectionReference().document();
        Machine machine = new Machine(reference.getId(), "Vélo elliptique");
        machine.addMuscle("Cardio");
        machine.addMuscle("Poids");
        machine.addMuscle("Haut du corps");
        machine.addMuscle("Bas du corps");
        machineRepository.create(machine);

        reference = machineRepository.getCollectionReference().document();
        machine = new Machine(reference.getId(), "Stepper");
        machine.addMuscle("Mollet");
        machine.addMuscle("Cuisses");
        machine.addMuscle("Fesses");
        machine.addMuscle("Fessiers");
        machineRepository.create(machine);

        reference = machineRepository.getCollectionReference().document();
        machine = new Machine(reference.getId(), "Rameur");
        machine.addMuscle("Bras");
        machine.addMuscle("Jambes");
        machine.addMuscle("Abdos");
        machine.addMuscle("Dos");
        machine.addMuscle("Pecs'");
        machineRepository.create(machine);

        reference = machineRepository.getCollectionReference().document();
        machine = new Machine(reference.getId(), "Vélo");
        machine.addMuscle("Mollet");
        machine.addMuscle("Cuisses");
        machine.addMuscle("Fesses");
        machine.addMuscle("Fessiers");
        machine.addMuscle("Jambes");
        machine.addMuscle("Abdos");
        machine.addMuscle("Hanches");
        machineRepository.create(machine);
    }
}
