package com.example.shareyourfit.data.repository;

import com.google.firebase.firestore.Exclude;

/**
 * @author https://gist.github.com/NiPfi/f00dc0f8178ef907202ed1046ced2746
 * Represents an object that can be uniquely identified among other objects of the same type
 * by using an UID.
 *
 * @param <TKey> type of the unique key (UID) this object is uniquely identified by. The type needs
 *              a correct implementation of its equals() method or the behaviour of code using this
 *              interface will be undefined.
 */
public interface Identifiable<TKey> {

    @Exclude
    TKey getEntityKey();

    void setEntityKey(TKey key);
}
