package com.example.shareyourfit.data.model.machine;

import androidx.annotation.NonNull;

import com.example.shareyourfit.data.repository.Identifiable;
import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.List;

public class Machine implements Identifiable<String> {
    private String machineID;
    private String name;
    private List<String> muscles;

    @Deprecated
    public Machine() {
        muscles = new ArrayList<>();
    }

    public Machine(String machineID, String name) {
        this.machineID = machineID;
        this.name = name;
        muscles = new ArrayList<>();
    }

    public void addMuscle(String muscle) {
        if (!muscles.contains(muscle))
            muscles.add(muscle);
    }

    public String getName() {
        return name;
    }

    public List<String> getMuscles() {
        return muscles;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Exclude
    @Override
    public String getEntityKey() {
        return machineID;
    }

    @Override
    public void setEntityKey(String s) {
        machineID = s;
    }
}
