package com.example.shareyourfit.data.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author https://gist.github.com/NiPfi/f00dc0f8178ef907202ed1046ced2746
 * Manages data access for Firebase
 */
public class FirestoreRepository<TEntity extends Identifiable<String>> implements Repository<TEntity, String> {

    private static final String TAG = "FirestoreRepository";

    private final Class<TEntity> entityClass;

    private final CollectionReference collectionReference;
    private final String collectionName;

    /**
     * Initializes the repository storing the data in the given collection.
     */
    public FirestoreRepository(Class<TEntity> entityClass, String collectionName) {
        this.collectionName = collectionName;
        this.entityClass = entityClass;

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        this.collectionReference = db.collection(this.collectionName);
    }

    public CollectionReference getCollectionReference() {
        return collectionReference;
    }

    public String getCollectionName() {
        return collectionName;
    }

    @Override
    public Task<Boolean> exists(final String documentName) {
        DocumentReference documentReference = collectionReference.document(documentName);
        Log.i(TAG, "Checking existence of '" + documentName + "' in '" + collectionName + "'.");

        return documentReference.get().continueWith(new Continuation<DocumentSnapshot, Boolean>() {
            @Override
            public Boolean then(@NonNull Task<DocumentSnapshot> task) {
                Log.d(TAG,"Checking if '" + documentName + "' exists in '" + collectionName +"'.");
                return Objects.requireNonNull(task.getResult()).exists();
            }
        });
    }

    @Override
    public Task<TEntity> get(String id) {
        final String documentName = id;
        DocumentReference documentReference = collectionReference.document(documentName);
        Log.i(TAG, "Getting '" + documentName + "' in '" + collectionName + "'.");

        return documentReference.get().continueWith(new Continuation<DocumentSnapshot, TEntity>() {
            @Override
            public TEntity then(@NonNull Task<DocumentSnapshot> task) throws Exception {
                DocumentSnapshot documentSnapshot = task.getResult();
                assert documentSnapshot != null;
                if (documentSnapshot.exists()) {
                    TEntity entity = documentSnapshot.toObject(entityClass);
                    assert entity != null;
                    entity.setEntityKey(documentSnapshot.getId());
                    return entity;
                } else {
                    Log.d(TAG, "Document '" + documentName + "' does not exist in '" + collectionName + "'.");
                    return entityClass.newInstance();
                }
            }
        });
    }

    @Override
    public Task<List<TEntity>> getAll() {
        Log.i(TAG, "Getting all entities in '" + collectionName + "'.");
        return collectionReference.get().continueWith(new Continuation<QuerySnapshot, List<TEntity>>() {
            @Override
            public List<TEntity> then(@NonNull Task<QuerySnapshot> task) {
                List<TEntity> entities = new ArrayList<>();
                for (QueryDocumentSnapshot snapshot : Objects.requireNonNull(task.getResult())) {
                    TEntity entity = snapshot.toObject(entityClass);
                    entity.setEntityKey(snapshot.getId());
                    entities.add(entity);
                }
                return entities;
            }
        });
    }

    @Override
    public Task<List<TEntity>> getByField(String field, Object value) {
        return collectionReference.whereEqualTo(field, value).get().continueWith(new Continuation<QuerySnapshot, List<TEntity>>() {
            @Override
            public List<TEntity> then(@NonNull Task<QuerySnapshot> task) {
                List<TEntity> entities = new ArrayList<>();
                for (QueryDocumentSnapshot snapshot : Objects.requireNonNull(task.getResult())) {
                    TEntity entity = snapshot.toObject(entityClass);
                    entity.setEntityKey(snapshot.getId());
                    entities.add(entity);
                }
                return entities;
            }
        });
    }

    @Override
    public Task<Void> create(TEntity entity) {
        final String documentName = entity.getEntityKey();
        DocumentReference documentReference = collectionReference.document(documentName);
        Log.i(TAG, "Creating '" + documentName + "' in '" + collectionName + "'.");
        return documentReference.set(entity).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "There was an error creating '" + documentName + "' in '" + collectionName + "'!", e);
            }
        });
    }

    @Override
    public Task<Void> update(TEntity entity) {
        final String documentName = entity.getEntityKey();
        DocumentReference documentReference = collectionReference.document(documentName);
        Log.i(TAG, "Updating '" + documentName + "' in '" + collectionName + "'.");

        return documentReference.set(entity).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "There was an error updating '" + documentName + "' in '" + collectionName + "'.", e);
            }
        });
    }

    @Override
    public Task<Void> delete(final String documentName) {
        DocumentReference documentReference = collectionReference.document(documentName);
        Log.i(TAG, "Deleting '" + documentName + "' in '" + collectionName + "'.");

        return documentReference.delete().addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "There was an error deleting '" + documentName + "' in '" + collectionName + "'.", e);
            }
        });
    }

}
