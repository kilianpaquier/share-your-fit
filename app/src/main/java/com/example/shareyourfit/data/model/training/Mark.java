package com.example.shareyourfit.data.model.training;

import com.example.shareyourfit.data.repository.Identifiable;
import com.google.firebase.firestore.Exclude;

import java.util.Objects;

public class Mark implements Identifiable<String> {
    private String markID;
    private String userID;
    private int nbStars;


    @Exclude
    @Override
    public String getEntityKey() {
        return markID;
    }

    @Override
    public void setEntityKey(String s) {
        markID = s;
    }

    public int getNbStars() {
        return nbStars;
    }

    public void setNbStars(int i) {
        nbStars = i;
    }

    public String getUserID() {
        return userID;
    }

    public String getMarkID() { return markID; }

    public Mark(){

    }

    public Mark(int nbStars, String userID){
        this.nbStars = nbStars;
        this.userID = userID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mark)) return false;
        Mark mark = (Mark) o;
        return markID.equals(mark.markID) && userID.equals(mark.userID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(markID, userID);
    }
}
