package com.example.shareyourfit.data.model.user;

import com.example.shareyourfit.data.model.training.MyTraining;
import com.example.shareyourfit.data.repository.Identifiable;
import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User implements Identifiable<String> {
    private String userID;
    private String firstName = null;
    private String lastName = null;
    private String bio = null;
    private Date birthDate;
    private String email;
    private String username;
    private Role role;
    private float reputation = -1;
    private List<String> publicatedTrainingsIDs;
    private List<String> myTrainingsIDs;

    @Deprecated
    public User() {
        publicatedTrainingsIDs = new ArrayList<>();
        myTrainingsIDs = new ArrayList<>();
    }

    public User(String userID, String username, String email, Date birthDate) {
        this.userID = userID;
        this.username = username;
        this.email = email;
        this.birthDate = birthDate;
        publicatedTrainingsIDs = new ArrayList<>();
        myTrainingsIDs = new ArrayList<>();
        role = Role.ADEPT;
    }

    public void addPublicatedTraining(String publicatedTrainingID) {
        if (!publicatedTrainingsIDs.contains(publicatedTrainingID)) { // Override du .equals() de publicatedTrainings
            publicatedTrainingsIDs.add(publicatedTrainingID);
        }
    }

    public void deletePublicatedTraining(String publicatedTrainingID) {
         // Override du .equals() de publicatedTrainings
            publicatedTrainingsIDs.remove(publicatedTrainingID);

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBio() {
        return bio;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public List<String> getPublicatedTrainingsIDs() {
        return publicatedTrainingsIDs;
    }

    public List<String> getMyTrainingsIDs() {
        return myTrainingsIDs;
    }

    public Role getRole() {
        return role;
    }

    public float getReputation() {
        return reputation;
    }

    @Exclude
    @Override
    public String getEntityKey() {
        return userID;
    }

    @Override
    public void setEntityKey(String s) {
        userID = s;
    }
}
