package com.example.shareyourfit.data.utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;

import com.example.shareyourfit.data.model.machine.Machine;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;



public abstract class QRCodeScan {
    // TODO Fonction pour lire un QRCode, changer le paramètre en entrée
    public static Machine scanQRCode() {
        return null;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void createQRCode(Machine machine) {
        File internalDcimDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        internalDcimDir = new File(internalDcimDir, "QRCODES");
        internalDcimDir.mkdir();
        internalDcimDir = new File(internalDcimDir, machine.getEntityKey() + ".PNG");

        try (FileOutputStream outputStream = new FileOutputStream(internalDcimDir)) {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(machine.getEntityKey(), BarcodeFormat.QR_CODE, 200, 200, null);
            int bitMatrixWidth = bitMatrix.getWidth();
            int bitMatrixHeight = bitMatrix.getHeight();
            int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

            int colorWhite = 0xFFFFFFFF;
            int colorBlack = 0xFF000000;

            for (int y = 0; y < bitMatrixHeight; y++) {
                int offset = y * bitMatrixWidth;
                for (int x = 0; x < bitMatrixWidth; x++) {
                    pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
            bitmap.setPixels(pixels, 0, 200, 0, 0, bitMatrixWidth, bitMatrixHeight);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        } catch (IllegalArgumentException | WriterException | IOException e) {
            e.printStackTrace();
        }
    }

}
