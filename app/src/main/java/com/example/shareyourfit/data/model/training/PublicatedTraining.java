package com.example.shareyourfit.data.model.training;

import android.net.Uri;

import com.example.shareyourfit.data.repository.Identifiable;
import com.google.firebase.firestore.Exclude;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PublicatedTraining implements Identifiable<String> {
    private String publicatedTrainingID;
    private String title;
    private String description;
    private String machineID;
    private List<String> tags;
    private List<Mark> marks;
    private float stars = -1;
    private String publicatorUsername;
    private Uri videoUri;
    private File videoFile;
    private boolean approved = false;

    @Deprecated
    public PublicatedTraining() {
        marks = new ArrayList<>();
        tags = new ArrayList<>();
    }

    public PublicatedTraining(String publicatedTrainingID, String title, String description, String machineID, String publicatorUsername) {
        this.publicatedTrainingID = publicatedTrainingID;
        this.title = title;
        this.description = description;
        this.machineID = machineID;
        this.publicatorUsername = publicatorUsername;
        marks = new ArrayList<>();
        tags = new ArrayList<>();
    }

    public void addTag(String tag) {
        if (!tags.contains(tag))
            tags.add(tag);
    }

    public void addMark(Mark mark) {
        if (!marks.contains(mark)) { // Override du .equals de Mark
            marks.add(mark);

            /*
            On calcule la note totale de la vidéo
             */
            int nbStars = 0;
            for (Mark mark1 : marks)
                nbStars += mark1.getNbStars();
            stars = (float) nbStars / marks.size();
        }
    }

    public void UpdateMark(Mark mark){
        if(marks.contains(mark)){
            marks.get(marks.indexOf(mark)).setNbStars(mark.getNbStars());

            /*
            On calcule la note totale de la vidéo
             */
            int nbStars = 0;
            for (Mark mark1 : marks)
                nbStars += mark1.getNbStars();
            stars = (float) nbStars / marks.size();
        }
    }

    public void removeMark(Mark mark){
        if(marks.contains(mark)){
            marks.remove(mark);

            /*
            On calcule la note totale de la vidéo
             */
            int nbStars = 0;
            for (Mark mark1 : marks)
                nbStars += mark1.getNbStars();
            stars = (float) nbStars / marks.size();
        }
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title=title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description=description;
    }

    public String getMachineID() {
        return machineID;
    }
    public void setMachineID(String machineId) {
        this.machineID=machineId;
    }

    public List<String> getTags() {
        return tags;
    }
    public void clearTags() {
        tags.clear();
    }

    public String getPublicatorUsername() {
        return publicatorUsername;
    }

    public float getStars() {
        return stars;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public void setVideoFile(File videoFile) {
        this.videoFile = videoFile;
    }

    public void setVideoUri(Uri videoUri) {
        this.videoUri = videoUri;
    }

    @Exclude
    public Uri getVideoUri() {
        return videoUri;
    }

    @Exclude
    public File getVideoFile() {
        return videoFile;
    }

    @Exclude
    @Override
    public String getEntityKey() {
        return publicatedTrainingID;
    }

    @Override
    public void setEntityKey(String s) {
        publicatedTrainingID = s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PublicatedTraining)) return false;
        PublicatedTraining that = (PublicatedTraining) o;
        return publicatedTrainingID.equals(that.publicatedTrainingID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publicatedTrainingID);
    }
}
