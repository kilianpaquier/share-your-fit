package com.example.shareyourfit.data.model.training;

import com.example.shareyourfit.data.repository.Identifiable;
import com.google.firebase.firestore.Exclude;

import java.util.List;

public class MyTraining implements Identifiable<String> {
    private String myTrainingID;
    private String trainingName;
    private List<String> exercisesIDs;

    @Exclude
    @Override
    public String getEntityKey() {
        return myTrainingID;
    }

    @Override
    public void setEntityKey(String s) {
        myTrainingID = s;
    }
}
