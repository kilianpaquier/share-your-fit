package com.example.shareyourfit.ui.training.ui.my_trainings_list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.training.PublicatedTraining;
import com.example.shareyourfit.data.model.user.Role;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.data.utils.QRCodeScanner;
import com.example.shareyourfit.ui.MainActivity;
import com.example.shareyourfit.ui.training.HomeActivity;
import com.example.shareyourfit.ui.training.ViewTrainingActivity;
import com.example.shareyourfit.ui.training.ui.modify_training.ModifyTrainingActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class MyTrainingsListAdapter extends RecyclerView.Adapter<MyTrainingsListAdapter.TrainingListAdapterHolder> {

    private FirestoreRepository<PublicatedTraining> publicatedTrainingRepo;
    private FirestoreRepository<User> mRepo;
    private List<PublicatedTraining> trainings;
    private StorageReference storageReference;
    private Context context;
    private User user;


    public MyTrainingsListAdapter(Context context, List<PublicatedTraining> trainings, User myUser) {

        this.context = context;
        this.trainings = trainings;
        this.user = myUser;
        publicatedTrainingRepo = new FirestoreRepository<>(PublicatedTraining.class, MainActivity.PUBLICATED_TRAININGS_REPO);
        storageReference = FirebaseStorage.getInstance().getReference();
        mRepo = new FirestoreRepository<>(User.class, MainActivity.USER_REPO);
    }

    public void setTrainings(List<PublicatedTraining> trainings) {
        this.trainings = trainings;
    }

    @NonNull
    @Override
    public TrainingListAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        view = LayoutInflater.from(context).inflate(R.layout.my_training_view, parent, false);
        return new TrainingListAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainingListAdapterHolder holder, int position) {
        holder.setDetails(trainings.get(position));
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    class TrainingListAdapterHolder extends RecyclerView.ViewHolder {
        private TextView titleText;
        private TextView usernameText;
        private ImageView imageView;
        private RatingBar ratingBar;
        private PublicatedTraining training;
        private ProgressBar progressBar;
        private TextView approvedText;
        private Button suppressButton;
        private Button modifyButton;
        private static final int MODIFY_RESULT =5;

        TrainingListAdapterHolder(@NonNull View itemView) {
            super(itemView);

            titleText = itemView.findViewById(R.id.title_training_text);
            usernameText = itemView.findViewById(R.id.username_text_video);
            imageView = itemView.findViewById(R.id.video_view);
            ratingBar = itemView.findViewById(R.id.rating_bar_video);
            progressBar = itemView.findViewById(R.id.image_progress_bar);
            suppressButton = itemView.findViewById(R.id.suppress_button);
            suppressButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setCancelable(true);
                    builder.setTitle("Suppression");
                    builder.setMessage("Voulez-vous vraiment supprimer cet entraînement.");
                    builder.setPositiveButton("Confirm",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    publicatedTrainingRepo.delete(training.getEntityKey()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                user.deletePublicatedTraining(training.getEntityKey());
                                                final DocumentReference reference = publicatedTrainingRepo.getCollectionReference().document();
                                                mRepo.update(user);
                                                storageReference.child(training.getEntityKey()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Toast.makeText(context, "vidéo supprimée", Toast.LENGTH_SHORT).show();
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception exception) {
                                                        Toast.makeText(context, "vidéo pas supprimée", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                                Toast.makeText(context, "entraînement supprimé", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Objects.requireNonNull(task.getException()).printStackTrace();
                                                Toast.makeText(context, "Impossible de supprimer l'entraînement", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            });
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            modifyButton = itemView.findViewById(R.id.modify_button);
            modifyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(context, ModifyTrainingActivity.class);
                    myIntent.putExtra("trainingId", training.getEntityKey()); //Optional parameters
                    myIntent.putExtra("machineId", training.getMachineID());
                    myIntent.putExtra("userId", user.getEntityKey());
                    context.startActivity(myIntent);
                }
            });

            if (user.getRole().equals(Role.COACH))
                approvedText = itemView.findViewById(R.id.approved_text);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ViewTrainingActivity.class);
                    intent.putExtra("videoId", training.getEntityKey());
                    context.startActivity(intent);
                }
            });
        }

        private void setDetails(PublicatedTraining publicatedTraining) {
            training = publicatedTraining;
            titleText.setText(publicatedTraining.getTitle());
            if (user.getRole().equals(Role.COACH)) {
                StringBuilder approveText = new StringBuilder();
                if (!publicatedTraining.isApproved()) {
                    approveText.append("Entraînement non approuvé");
                    this.approvedText.setTextColor(Color.RED);
                }
                else {
                    approveText.append("Entraînement approuvé");
                    this.approvedText.setTextColor(Color.GREEN);
                }
                this.approvedText.setText(approveText.toString());
            }
            usernameText.setText(publicatedTraining.getPublicatorUsername());
            if (publicatedTraining.getStars() == -1)
                ratingBar.setNumStars(0);
            else
                ratingBar.setRating(publicatedTraining.getStars());
            // imageView.setVideoURI(publicatedTraining.getVideoUri());
            // imageView.seekTo(1);
            if (publicatedTraining.getVideoFile() != null) {
                imageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(publicatedTraining.getVideoFile().getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND));
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
