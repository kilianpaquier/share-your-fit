package com.example.shareyourfit.ui.training;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.ui.MainActivity;
import com.example.shareyourfit.ui.login.LoginActivity;
import com.example.shareyourfit.ui.training.ui.logout.LogoutFragment;
import com.example.shareyourfit.ui.training.ui.new_training.NewTrainingFragment;
import com.example.shareyourfit.ui.training.ui.profile.ProfileFragment;
import com.example.shareyourfit.ui.training.ui.my_trainings_list.MyTrainingsListFragment;
import com.example.shareyourfit.ui.training.ui.training_list.TrainingListFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;
import java.util.Objects;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int UPDATE_USERNAME_EMAIL_VIEWS = 5000;
    private Handler handler;

    private TextView usernameView;
    private TextView emailView;
    private NavigationView navigationView;
    private DrawerLayout drawer;

    private FirebaseAuth firebaseAuth;
    private User myUser;
    private FirestoreRepository<User> userRepo;
    private Task<User> userTask;

    public Task<User> getUserTask() {
        return userTask;
    }

    public User getMyUser() {
        return myUser;
    }

    public FirestoreRepository<User> getUserRepo() {
        return userRepo;
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setHomeAsUpIndicator(R.drawable.ic_home_menu);
        actionbar.setDisplayHomeAsUpEnabled(true);
        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_training_list);

        firebaseAuth = FirebaseAuth.getInstance();
        userRepo = new FirestoreRepository<>(User.class, MainActivity.USER_REPO);

        handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if (msg.what == UPDATE_USERNAME_EMAIL_VIEWS) {
                    if (myUser != null && usernameView != null && emailView != null) {
                        usernameView.setText(myUser.getUsername());
                        emailView.setText(myUser.getEmail());
                    }
                }
            }
        };

        FirebaseUser user = firebaseAuth.getCurrentUser();
        updateUI(user);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void updateUI(FirebaseUser firebaseUser) {
        if (firebaseUser == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            userTask = userRepo.get(firebaseUser.getUid()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        myUser = task.getResult();
                        Message message = new Message();
                        message.what = UPDATE_USERNAME_EMAIL_VIEWS;
                        handler.sendMessage(message);
                    } else {
                        Objects.requireNonNull(task.getException()).printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        drawer.openDrawer(GravityCompat.START);

        if (usernameView == null)
            usernameView = findViewById(R.id.username_nav_text);
        if (emailView == null)
            emailView = findViewById(R.id.email_text);
        Message message = new Message();
        message.what = UPDATE_USERNAME_EMAIL_VIEWS;
        handler.sendMessage(message);

        return true;
        // return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    /**
     * Change the current displayed fragment by a new one.
     * - if the fragment is in backstack, it will pop it
     * - if the fragment is already displayed (trying to change the fragment with the same), it will not do anything
     *
     * @param frag            the new fragment to display
     * @param saveInBackstack if we want the fragment to be in backstack
     * @param animate         if we want a nice animation or not
     */
    public void changeFragment(Fragment frag, boolean saveInBackstack, boolean animate) {
        String backStateName = ((Object) frag).getClass().getName();

        try {
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
                //fragment not in back stack, create it.
                FragmentTransaction transaction = manager.beginTransaction();

                List<Fragment> fragments = manager.getFragments();
                if (!fragments.isEmpty()) {
                    for (Fragment fragment : fragments)
                        transaction.remove(fragment);
                }

                if (animate) {
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }

                // transaction.remove(oldFrag).commit();
                transaction.replace(R.id.nav_host_fragment, frag, backStateName);

                if (saveInBackstack) {
                    transaction.addToBackStack(backStateName);
                }

                transaction.commit();
            }  // custom effect if fragment is already instanciated

        } catch (IllegalStateException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_add_video:
                changeFragment(new NewTrainingFragment(), false, true);
                break;
            case R.id.nav_logout:
                changeFragment(new LogoutFragment(), false, true);
                break;
            case R.id.nav_profile:
                changeFragment(new ProfileFragment(), false, true);
                break;
            case R.id.nav_training_list:
                changeFragment(new TrainingListFragment(), false, true);
                break;
            case R.id.nav_my_trainnings_list:
                changeFragment(new MyTrainingsListFragment(), false, true);
                break;
        }
        navigationView.setCheckedItem(item);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
