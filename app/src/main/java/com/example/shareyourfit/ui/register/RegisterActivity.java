package com.example.shareyourfit.ui.register;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.ui.MainActivity;
import com.example.shareyourfit.ui.login.LoginActivity;
import com.example.shareyourfit.ui.training.HomeActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText usernameEdit;
    private EditText birthDateEdit;
    private EditText emailEdit;
    private EditText passwordEdit;
    private EditText passwordRepeatEdit;

    private FirebaseAuth firebaseAuth;
    private FirestoreRepository<User> userRepository;
    private GoogleSignInClient googleSignInClient;
    private CallbackManager callbackManager;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameEdit = findViewById(R.id.username_text_register);
        birthDateEdit = findViewById(R.id.birth_date);
        emailEdit = findViewById(R.id.email_text);
        passwordEdit = findViewById(R.id.password_text);
        passwordRepeatEdit = findViewById(R.id.password_repeat_text);

        firebaseAuth = FirebaseAuth.getInstance();
        userRepository = new FirestoreRepository<>(User.class, MainActivity.USER_REPO);

        /*
        Google sign up
         */
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        findViewById(R.id.google_sign_up).setOnClickListener(this);

        /*
        Facebook sign up
         */
        callbackManager = CallbackManager.Factory.create();
        LoginButton fbLoginButton = findViewById(R.id.facebook_sign_up);
        fbLoginButton.setPermissions("email");

        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(RegisterActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        updateUI(user);
    }

    /**
     * NB : Les regex ont été récupérés en ligne
     *
     * @param v Le bouton responsable de l'appel à la fonction
     */
    @SuppressLint("SimpleDateFormat")
    public void onClickSignUp(View v) {
        final String username = usernameEdit.getText().toString();
        final String birthDate = birthDateEdit.getText().toString();
        final String email = emailEdit.getText().toString();
        final String password = passwordEdit.getText().toString();
        final String passwordRepeat = passwordRepeatEdit.getText().toString();
        Date dateBirthDate = null;

        /*
        Vérification des champs obligatoires
         */
        if (username.trim().isEmpty() || email.trim().isEmpty() || password.trim().isEmpty() || passwordRepeat.trim().isEmpty()) {
            Toast.makeText(this, "Un des champs requis n'a pas été saisi", Toast.LENGTH_SHORT).show();
            return;
        }

        /*
        Vérification de la date de naissance
         */
        if (!birthDate.trim().isEmpty() && !Pattern.matches("^(0?[1-9]|[12][0-9]|3[01])[\\/\\-](0?[1-9]|1[012])[\\/\\-]\\d{4}$", birthDate)) {
            birthDateEdit.setTextColor(Color.RED);
            Toast.makeText(this, "Date de naissance invalide", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            if (!birthDate.trim().isEmpty())
                dateBirthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate);
        } catch (ParseException e) {
            Toast.makeText(this, "Date de naissance invalide", Toast.LENGTH_SHORT).show();
            return;
        }
        final Date finalDateBirthDate = dateBirthDate;
        birthDateEdit.setTextColor(Color.BLACK);

        /*
        Vérification de l'adresse email
         */
        if (!Pattern.matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", email)) {
            emailEdit.setTextColor(Color.RED);
            Toast.makeText(this, "Adresse email invalide", Toast.LENGTH_SHORT).show();
            return;
        }
        emailEdit.setTextColor(Color.BLACK);

        /*
        Vérification de la similitude des deux mots de passe
         */
        if (!password.equals(passwordRepeat)) {
            passwordRepeatEdit.setTextColor(Color.RED);
            Toast.makeText(this, "Les deux mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show();
            return;
        }
        passwordRepeatEdit.setTextColor(Color.BLACK);

        /*
        Sauvegarde de l'utilisateur
         */
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User adept = new User(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid(), username, email, finalDateBirthDate);
                    userRepository.create(adept);
                    updateUI(firebaseAuth.getCurrentUser());
                } else
                    Toast.makeText(RegisterActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            Toast.makeText(this, "Bienvenue sur Share Your Fit !", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) { // Pour google
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        } else { // Pour facebook
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser user = firebaseAuth.getCurrentUser();
                    assert user != null;
                    final User adept = new User(user.getUid(), user.getDisplayName(), user.getEmail(), null);

                    userRepository.exists(user.getUid()).addOnCompleteListener(new OnCompleteListener<Boolean>() {
                        @Override
                        public void onComplete(@NonNull Task<Boolean> task) {
                            if (task.isSuccessful()) {
                                if (!Objects.requireNonNull(task.getResult())) {
                                    userRepository.create(adept);
                                }
                                updateUI(user);
                            } else {
                                Toast.makeText(RegisterActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else
                    Toast.makeText(RegisterActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser user = firebaseAuth.getCurrentUser();
                    assert user != null;
                    final User adept = new User(user.getUid(), user.getDisplayName(), user.getEmail(), null);

                    userRepository.exists(user.getUid()).addOnCompleteListener(new OnCompleteListener<Boolean>() {
                        @Override
                        public void onComplete(@NonNull Task<Boolean> task) {
                            if (task.isSuccessful()) {
                                if (!Objects.requireNonNull(task.getResult())) {
                                    userRepository.create(adept);
                                }
                                updateUI(user);
                            } else {
                                Toast.makeText(RegisterActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    if (Objects.requireNonNull(task.getException()).getClass().equals(FirebaseAuthUserCollisionException.class))
                        Toast.makeText(RegisterActivity.this, "Adresse email déjà utilisée par un autre compte", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onClickSignIn(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void finishActivity(View v) {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.google_sign_up) {
            Intent intent = googleSignInClient.getSignInIntent();
            startActivityForResult(intent, RC_SIGN_IN);
        }
    }
}
