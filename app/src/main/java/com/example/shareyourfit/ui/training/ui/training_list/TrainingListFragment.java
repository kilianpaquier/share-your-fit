package com.example.shareyourfit.ui.training.ui.training_list;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.training.PublicatedTraining;
import com.example.shareyourfit.data.model.user.Role;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.data.utils.QRCodeScanner;
import com.example.shareyourfit.ui.MainActivity;
import com.example.shareyourfit.ui.training.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TrainingListFragment extends NavHostFragment {

    private FirestoreRepository<PublicatedTraining> publicatedTrainingRepo;
    private StorageReference storageReference;
    private List<PublicatedTraining> trainings;
    private List<PublicatedTraining> resultOfResearch;
    private RecyclerView recyclerView;
    private TrainingListAdapter recycleAdapter;
    private SwipeRefreshLayout layout;
    private HomeActivity homeActivity;
    private String ResultQRCode;
    private static final int QR_CODE_RESULT =2;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_training_list, container, false);
        publicatedTrainingRepo = new FirestoreRepository<>(PublicatedTraining.class, MainActivity.PUBLICATED_TRAININGS_REPO);
        storageReference = FirebaseStorage.getInstance().getReference();

        /*
        Configuration du recycleView
         */
        recyclerView = root.findViewById(R.id.training_list_recycle_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager recycleLayoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(recycleLayoutManager);


        layout = root.findViewById(R.id.refresh_layout);
        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(resultOfResearch!=null) {
                    if (resultOfResearch.isEmpty()) {
                        loadVideos();
                    } else {
                        initList(resultOfResearch);
                    }
                }
                else
                {
                    loadVideos();
                }

            }
        });

        EditText researchText = root.findViewById(R.id.search_text);
        researchText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length()==0)
                {
                    loadVideos();
                }
                else{
                    resultOfResearch = new ArrayList<>();
                    String text = s.toString();
                    for (PublicatedTraining pt : trainings) {
                        for(String tag : pt.getTags())
                        {
                            if(text.toLowerCase().contains(tag.toLowerCase()))
                            {
                                resultOfResearch.add(pt);
                            }
                        }

                    }
                    initList(resultOfResearch);
                }

            }
        });

        ImageButton QrCodeButton = root.findViewById(R.id.qr_code_button);
        QrCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), QRCodeScanner.class);
                //myIntent.putExtra("key", value); //Optional parameters
                startActivityForResult(myIntent, QR_CODE_RESULT);

            }
        });
        homeActivity = (HomeActivity) requireActivity();

        if (homeActivity.getMyUser() == null) {
            homeActivity.getUserTask().addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    loadVideos();
                }
            });
        } else {
            loadVideos();
        }

        return root;
    }

    private void loadVideos() {
        // final ProgressDialog dialog = ProgressDialog.show(requireContext(), "Chargement", "Chargement des vidéos...", true);
        // dialog.show();

        if (homeActivity.getMyUser().getRole().equals(Role.ADEPT)) {
            publicatedTrainingRepo.getByField("approved", true).addOnCompleteListener(new OnCompleteListener<List<PublicatedTraining>>() {
                @Override
                public void onComplete(@NonNull Task<List<PublicatedTraining>> task) {
                    trainings = new ArrayList<>();
                    if (task.isSuccessful()) {
                        trainings = task.getResult();
                        assert trainings != null;
                        for (int count = 0; count < trainings.size(); count++) {
                            final PublicatedTraining training = trainings.get(count);
                            final File localFile = createTempFile(training.getEntityKey(), ".mp4");
                            storageReference.child(training.getEntityKey()).getFile(localFile).addOnCompleteListener(new OnCompleteListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<FileDownloadTask.TaskSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        training.setVideoFile(localFile);
                                        training.setVideoUri(Uri.fromFile(localFile));
                                        recycleAdapter.notifyDataSetChanged();
                                    } else {
                                        Objects.requireNonNull(task.getException()).printStackTrace();
                                    }
                                }
                            });
                        }
                    } else {
                        Objects.requireNonNull(task.getException()).printStackTrace();
                    }
                    initList(trainings);
                }
            });
        } else {
            publicatedTrainingRepo.getAll().addOnCompleteListener(new OnCompleteListener<List<PublicatedTraining>>() {
                @Override
                public void onComplete(@NonNull Task<List<PublicatedTraining>> task) {
                    trainings = new ArrayList<>();
                    if (task.isSuccessful()) {
                        trainings = task.getResult();
                        assert trainings != null;
                        for (int count = 0; count < trainings.size(); count++) {
                            final PublicatedTraining training = trainings.get(count);
                            final File localFile = createTempFile(training.getEntityKey(), ".mp4");
                            storageReference.child(training.getEntityKey()).getFile(localFile).addOnCompleteListener(new OnCompleteListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<FileDownloadTask.TaskSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        training.setVideoFile(localFile);
                                        training.setVideoUri(Uri.fromFile(localFile));
                                        recycleAdapter.notifyDataSetChanged();
                                    } else {
                                        Objects.requireNonNull(task.getException()).printStackTrace();
                                    }
                                }
                            });
                        }
                    } else {
                        Objects.requireNonNull(task.getException()).printStackTrace();
                    }
                    initList(trainings);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initList(List<PublicatedTraining> publicatedTrainings) {
        if (recycleAdapter == null) {
            recycleAdapter = new TrainingListAdapter(requireContext(), publicatedTrainings, homeActivity.getMyUser());
            recyclerView.setAdapter(recycleAdapter);
        } else {
            recycleAdapter.setTrainings(publicatedTrainings);
            recycleAdapter.notifyDataSetChanged();
            layout.setRefreshing(false);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static File createTempFile(String prefix, String suffix) {
        File parent = new File(Objects.requireNonNull(System.getProperty("java.io.tmpdir")));

        File temp = new File(parent, prefix + suffix);

        if (temp.exists()) {
            temp.delete();
        }

        try {
            temp.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return temp;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (QR_CODE_RESULT) : {
                if (resultCode == Activity.RESULT_OK) {
                    ResultQRCode = data.getStringExtra("QRCode");
                    if(ResultQRCode!=null)
                    {
                        if(ResultQRCode.length()>0) {
                            resultOfResearch = new ArrayList<>();
                            for (PublicatedTraining pt : trainings) {
                                if (pt.getMachineID().equals(ResultQRCode)) {
                                    resultOfResearch.add(pt);
                                }
                            }
                            initList(resultOfResearch);
                        }
                    }


                }
                break;
            }
        }
    }
}
