package com.example.shareyourfit.ui.training.ui.new_training;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.navigation.fragment.NavHostFragment;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.machine.Machine;
import com.example.shareyourfit.data.model.training.PublicatedTraining;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.ui.MainActivity;
import com.example.shareyourfit.ui.training.HomeActivity;
import com.example.shareyourfit.ui.training.ui.training_list.TrainingListFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class NewTrainingFragment extends NavHostFragment {

    private static final int GALLERY = 1;
    private static final int CAMERA = 2;
    private static final String VIDEO_DIRECTORY = "/shareyourfit";
    private static final int MY_PERMISSIONS_REQUEST_USEFULL = 5000;

    private File video = null;

    private EditText titleEdit;
    private EditText descriptionEdit;
    private EditText tagsEdit;
    private HomeActivity homeActivity;

    private StorageReference storageReference;
    private FirestoreRepository<Machine> machineRepository;
    private FirestoreRepository<PublicatedTraining> publicatedTrainingRepository;

    private Spinner machineSpinner;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_new_training, container, false);

        titleEdit = root.findViewById(R.id.video_name);
        descriptionEdit = root.findViewById(R.id.video_description);
        tagsEdit = root.findViewById(R.id.tag_edit);

        /*
        On check les permissions nécessaires
         */
        if (ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_USEFULL);
        }

        machineSpinner = root.findViewById(R.id.spinner_machines);
        root.findViewById(R.id.upload_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadVideo();
            }
        });
        root.findViewById(R.id.save_training_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTraining();
            }
        });

        storageReference = FirebaseStorage.getInstance().getReference();
        machineRepository = new FirestoreRepository<>(Machine.class, MainActivity.MACHINE_REPO);
        publicatedTrainingRepository = new FirestoreRepository<>(PublicatedTraining.class, MainActivity.PUBLICATED_TRAININGS_REPO);
        populateMachineList();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        homeActivity = (HomeActivity) requireActivity();
    }

    private void populateMachineList() {
        machineRepository.getAll().addOnCompleteListener(new OnCompleteListener<List<Machine>>() {
            @Override
            public void onComplete(@NonNull Task<List<Machine>> task) {
                if (task.isSuccessful()) {
                    ArrayAdapter<Machine> arrayAdapter = new ArrayAdapter<Machine>(homeActivity.getBaseContext(), R.layout.simple_spinner_item, Objects.requireNonNull(task.getResult()));
                    arrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    machineSpinner.setAdapter(arrayAdapter);
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });
    }

    private void uploadVideo() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(requireContext());
        pictureDialog.setTitle("Action de téléchargement");
        String[] pictureDialogItems = {"Téléchargement depuis la galerie", "Enregistrement depuis la caméra"};
        pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, GALLERY);
                        break;
                    case 1:
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        startActivityForResult(intent, CAMERA);
                        break;
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_USEFULL) {
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED || grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Pour ajouter des entraînements vous devez autoriser l'accès aux fichiers et à la caméra", Toast.LENGTH_LONG).show();

                homeActivity.changeFragment(new TrainingListFragment(), false, true);
                homeActivity.getNavigationView().setCheckedItem(R.id.nav_training_list);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                String selectedVideoPath = getPath(contentURI);
                try {
                    video = saveVideoToInternalStorage(selectedVideoPath);
                    Toast.makeText(requireContext(), "Vidéo téléchargée", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(requireContext(), "Erreur lors de la récupération de la vidéo, réessayez plus tard", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        } else if (requestCode == CAMERA) {
            if (data != null) {
                Uri contentURI = data.getData();
                String recordedVideoPath = getPath(contentURI);
                try {
                    video = saveVideoToInternalStorage(recordedVideoPath);
                    Toast.makeText(requireContext(), "Vidéo téléchargée", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(requireContext(), "Erreur lors de l'enregistrement de la vidéo, réessayez plus tard", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    private void saveTraining() {
        final DocumentReference reference = publicatedTrainingRepository.getCollectionReference().document();
        final String title = titleEdit.getText().toString();
        final String description = descriptionEdit.getText().toString();
        final String tagString = tagsEdit.getText().toString();
        final Machine machine = (Machine) machineSpinner.getSelectedItem();

        if (title.trim().isEmpty() || description.trim().isEmpty() || video == null || tagString.trim().isEmpty()) {
            Toast.makeText(requireContext(), "Veuillez saisir un titre, une description, des tags et la vidéo pour l'entraînement", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog dialog = ProgressDialog.show(requireContext(), "Chargement", "Publication de l'entraînement en ligne...", true);
        dialog.show();
        storageReference.child(reference.getId()).putFile(Uri.fromFile(video)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    final PublicatedTraining publicatedTraining = new PublicatedTraining(reference.getId(), title, description, machine.getEntityKey(), ((HomeActivity) requireActivity()).getMyUser().getUsername());

                    /*
                    On ajoute les tags à partir de la chaîne récupérée
                     */
                    for (String tag : tagString.split("#")) {
                        tag = tag.replaceAll(" ", ""); // trim() ne retirant qu'à la fin et au début
                        if (tag.isEmpty())
                            continue;
                        else
                            tag = "#" + tag;
                        publicatedTraining.addTag(tag);
                    }

                    final User myUser = ((HomeActivity) requireActivity()).getMyUser();
                    myUser.addPublicatedTraining(publicatedTraining.getEntityKey());
                    publicatedTrainingRepository.create(publicatedTraining).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                homeActivity.getUserRepo().update(myUser);
                                Toast.makeText(requireContext(), "Entraînement publié", Toast.LENGTH_SHORT).show();
                                homeActivity.changeFragment(new TrainingListFragment(), false, true);
                                homeActivity.getNavigationView().setCheckedItem(R.id.nav_training_list);
                            } else {
                                Objects.requireNonNull(task.getException()).printStackTrace();
                            }
                        }
                    });
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
                dialog.dismiss();
            }
        });
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private File saveVideoToInternalStorage(String filePath) throws IOException {
        File currentFile = new File(filePath);
        File videoDirectory = new File(Environment.getExternalStorageDirectory() + VIDEO_DIRECTORY);
        File newFile = new File(videoDirectory, Calendar.getInstance().getTimeInMillis() + ".mp4");

        if (!videoDirectory.exists()) {
            videoDirectory.mkdirs();
        }

        if (currentFile.exists()) {
            try (InputStream in = new FileInputStream(currentFile); OutputStream out = new FileOutputStream(newFile)) {
                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
        return newFile;
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        try (Cursor cursor = requireActivity().getContentResolver().query(uri, projection, null, null, null)) {
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else
                return null;
        }
    }
}
