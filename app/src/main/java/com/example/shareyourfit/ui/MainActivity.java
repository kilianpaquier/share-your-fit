package com.example.shareyourfit.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.shareyourfit.R;
import com.example.shareyourfit.ui.login.LoginActivity;
import com.example.shareyourfit.ui.register.RegisterActivity;
import com.example.shareyourfit.ui.training.HomeActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    public static final String USER_REPO = "users";
    public static final String MACHINE_REPO = "machines";
    public static final String PUBLICATED_TRAININGS_REPO = "publicated_trainings";

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // new MachineCreator();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        updateUI(user);
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            // Toast.makeText(this, "Bienvenue sur Share Your Fit !", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
    }

    public void onClickSignUp(View v) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void onClickSignIn(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
