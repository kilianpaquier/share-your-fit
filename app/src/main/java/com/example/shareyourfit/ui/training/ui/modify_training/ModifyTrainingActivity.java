package com.example.shareyourfit.ui.training.ui.modify_training;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.machine.Machine;
import com.example.shareyourfit.data.model.training.PublicatedTraining;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.ui.MainActivity;
import com.example.shareyourfit.ui.training.HomeActivity;
import com.example.shareyourfit.ui.training.ui.training_list.TrainingListFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;


public class ModifyTrainingActivity extends AppCompatActivity {

    private static final int GALLERY = 1;
    private static final int CAMERA = 2;
    private static final String VIDEO_DIRECTORY = "/shareyourfit";
    private static final int MY_PERMISSIONS_REQUEST_USEFULL = 5000;

    private File video = null;
    private User user;
    private EditText titleEdit;
    private EditText descriptionEdit;
    private EditText tagsEdit;
    private String trainingId;
    private PublicatedTraining training;
    private String machineId;
    private Machine machine;

    private StorageReference storageReference;
    private FirestoreRepository<Machine> machineRepository;
    private FirestoreRepository<User> mRepo;
    private FirestoreRepository<PublicatedTraining> publicatedTrainingRepository;
    private Context context;

    private boolean newVideoUpload = false;

    private Spinner machineSpinner;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        context = this;
        setContentView(R.layout.activity_modify_training);
        trainingId = getIntent().getStringExtra("trainingId");
        machineId = getIntent().getStringExtra("machineId");
        titleEdit = findViewById(R.id.video_name);
        descriptionEdit = findViewById(R.id.video_description);
        tagsEdit = findViewById(R.id.tag_edit);




        /*
        On check les permissions nécessaires
         */
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_USEFULL);
        }

        machineSpinner = findViewById(R.id.spinner_machines);
        findViewById(R.id.upload_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadVideo();
            }
        });
        findViewById(R.id.save_training_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTraining();
            }
        });

        storageReference = FirebaseStorage.getInstance().getReference();
        machineRepository = new FirestoreRepository<>(Machine.class, MainActivity.MACHINE_REPO);
        mRepo = new FirestoreRepository<>(User.class, MainActivity.USER_REPO);
        String idUser = getIntent().getStringExtra("userId");
        mRepo.get(idUser).addOnCompleteListener(new OnCompleteListener<User>() {
            @Override
            public void onComplete(@NonNull Task<User> task) {
                if (task.isSuccessful()) {
                    user = Objects.requireNonNull(task.getResult());
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });
        publicatedTrainingRepository = new FirestoreRepository<>(PublicatedTraining.class, MainActivity.PUBLICATED_TRAININGS_REPO);

        publicatedTrainingRepository.get(trainingId).addOnCompleteListener(new OnCompleteListener<PublicatedTraining>() {
            @Override
            public void onComplete(@NonNull Task<PublicatedTraining> task) {
                if (task.isSuccessful()) {
                    training = Objects.requireNonNull(task.getResult());
                    titleEdit.setText(training.getTitle());
                    descriptionEdit.setText(training.getDescription());
                    StringBuilder tags = new StringBuilder();
                    for(String tag : training.getTags())
                    {
                        tags.append(tag);
                    }
                    tagsEdit.setText(tags);
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });
        populateMachineList();
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void populateMachineList() {
        machineRepository.get(machineId).addOnCompleteListener(new OnCompleteListener<Machine>() {
            @Override
            public void onComplete(@NonNull Task<Machine> task) {
                if (task.isSuccessful()) {
                    machine = Objects.requireNonNull(task.getResult());
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });
        machineRepository.getAll().addOnCompleteListener(new OnCompleteListener<List<Machine>>() {
            @Override
            public void onComplete(@NonNull Task<List<Machine>> task) {
                if (task.isSuccessful()) {
                    ArrayAdapter<Machine> arrayAdapter = new ArrayAdapter<>(context, R.layout.simple_spinner_item, Objects.requireNonNull(task.getResult()));
                    arrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    machineSpinner.setAdapter(arrayAdapter);
                    machineSpinner.setSelection(arrayAdapter.getPosition(machine));
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });
    }
    private void uploadVideo() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Action de téléchargement");
        String[] pictureDialogItems = {"Téléchargement depuis la galerie", "Enregistrement depuis la caméra"};
        pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, GALLERY);
                        break;
                    case 1:
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        startActivityForResult(intent, CAMERA);
                        break;
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_USEFULL) {
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED || grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Pour ajouter des entraînements vous devez autoriser l'accès aux fichiers et à la caméra", Toast.LENGTH_LONG).show();

                finish();
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                String selectedVideoPath = getPath(contentURI);
                try {
                    video = saveVideoToInternalStorage(selectedVideoPath);
                    newVideoUpload = true;
                    Toast.makeText(this, "Vidéo téléchargée", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(this, "Erreur lors de la récupération de la vidéo, réessayez plus tard", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        } else if (requestCode == CAMERA) {
            if (data != null) {
                Uri contentURI = data.getData();
                String recordedVideoPath = getPath(contentURI);
                try {
                    video = saveVideoToInternalStorage(recordedVideoPath);
                    newVideoUpload = true;
                    Toast.makeText(this, "Vidéo téléchargée", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(this, "Erreur lors de l'enregistrement de la vidéo, réessayez plus tard", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateTraining() {
        final DocumentReference reference = publicatedTrainingRepository.getCollectionReference().document();
        final String title = titleEdit.getText().toString();
        final String description = descriptionEdit.getText().toString();
        final String tagString = tagsEdit.getText().toString();
        final Machine machine = (Machine) machineSpinner.getSelectedItem();

        if (title.trim().isEmpty() || description.trim().isEmpty() || tagString.trim().isEmpty()) {
            Toast.makeText(this, "Veuillez saisir un titre, une description, des tags et la vidéo pour l'entraînement", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog dialog = ProgressDialog.show(this, "Chargement", "Publication de l'entraînement en ligne...", true);
        dialog.show();

        if(newVideoUpload){
            /*on supprime l'ancienne vidéo*/
            storageReference.child(training.getEntityKey()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    /*on supprime l'ancienne publication*/
                    publicatedTrainingRepository.delete(trainingId).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                user.deletePublicatedTraining(trainingId);
                                mRepo.update(user);
                            }else
                            {
                                Objects.requireNonNull(task.getException()).printStackTrace();
                            }
                        }
                    });
                    Toast.makeText(context, "vidéo supprimée", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(context, "vidéo pas supprimée", Toast.LENGTH_SHORT).show();
                }
            });
            /*on ajoute la nouvelle*/
            storageReference.child(training.getEntityKey()).putFile(Uri.fromFile(video)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if (task.isSuccessful()) {
                        final User myUser = user;

                        final PublicatedTraining publicatedTraining = new PublicatedTraining(training.getEntityKey(), title, description, machine.getEntityKey(), user.getUsername());

                    /*
                    On ajoute les tags à partir de la chaîne récupérée
                     */
                        for (String tag : tagString.split("#")) {
                            tag = tag.replaceAll(" ", ""); // trim() ne retirant qu'à la fin et au début
                            if (tag.isEmpty())
                                continue;
                            else
                                tag = "#" + tag;
                            publicatedTraining.addTag(tag);
                        }


                        myUser.addPublicatedTraining(publicatedTraining.getEntityKey());
                        publicatedTrainingRepository.create(publicatedTraining).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    mRepo.update(myUser);
                                    Toast.makeText(context, "Entraînement publié", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Objects.requireNonNull(task.getException()).printStackTrace();
                                }
                            }
                        });
                    } else {
                        Objects.requireNonNull(task.getException()).printStackTrace();
                    }
                    dialog.dismiss();
                    finish();
                }
            });
        }
        else{
            publicatedTrainingRepository.get(trainingId).addOnCompleteListener(new OnCompleteListener<PublicatedTraining>() {
                @Override
                public void onComplete(@NonNull Task<PublicatedTraining> task) {
                    if (task.isSuccessful()) {
                        final PublicatedTraining publicatedTraining = Objects.requireNonNull(task.getResult());
                        publicatedTraining.setTitle(title);
                        publicatedTraining.setDescription(description);
                        publicatedTraining.setMachineID(machineId);
                        publicatedTraining.clearTags();
                        /*
                    On ajoute les tags à partir de la chaîne récupérée
                     */
                        for (String tag : tagString.split("#")) {
                            tag = tag.replaceAll(" ", ""); // trim() ne retirant qu'à la fin et au début
                            if (tag.isEmpty())
                                continue;
                            else
                                tag = "#" + tag;
                            publicatedTraining.addTag(tag);
                        }
                        publicatedTrainingRepository.update(publicatedTraining);
                        finish();

                    } else {
                        Objects.requireNonNull(task.getException()).printStackTrace();
                    }
                }
            });
            dialog.dismiss();

        }


    }
    private void DeleteVideo(File videofile)
    {
        if (videofile.exists()) {
            if (videofile.delete()) {
                System.out.println("file Deleted :" );
            } else {
                System.out.println("file not Deleted :" );
            }
        }
    }
    private File saveVideoToInternalStorage(String filePath) throws IOException {
        File currentFile = new File(filePath);
        File videoDirectory = new File(Environment.getExternalStorageDirectory() + VIDEO_DIRECTORY);
        File newFile = new File(videoDirectory, Calendar.getInstance().getTimeInMillis() + ".mp4");

        if (!videoDirectory.exists()) {
            videoDirectory.mkdirs();
        }

        if (currentFile.exists()) {
            try (InputStream in = new FileInputStream(currentFile); OutputStream out = new FileOutputStream(newFile)) {
                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
        return newFile;
    }
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        try (Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null)) {
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else
                return null;
        }
    }
}
