package com.example.shareyourfit.ui.training.ui.training_list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.training.PublicatedTraining;
import com.example.shareyourfit.data.model.user.Role;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.ui.training.ViewTrainingActivity;

import java.util.List;

public class TrainingListAdapter extends RecyclerView.Adapter<TrainingListAdapter.TrainingListAdapterHolder> {

    private List<PublicatedTraining> trainings;
    private Context context;
    private User user;

    public TrainingListAdapter(Context context, List<PublicatedTraining> trainings, User myUser) {
        this.context = context;
        this.trainings = trainings;
        this.user = myUser;
    }

    public void setTrainings(List<PublicatedTraining> trainings) {
        this.trainings = trainings;
    }

    @NonNull
    @Override
    public TrainingListAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (user.getRole().equals(Role.COACH))
            view = LayoutInflater.from(context).inflate(R.layout.training_view_coach, parent, false);
        else
            view = LayoutInflater.from(context).inflate(R.layout.training_view, parent, false);
        return new TrainingListAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainingListAdapterHolder holder, int position) {
        holder.setDetails(trainings.get(position));
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    class TrainingListAdapterHolder extends RecyclerView.ViewHolder {
        private TextView titleText;
        private TextView usernameText;
        private ImageView imageView;
        private RatingBar ratingBar;
        private PublicatedTraining training;
        private ProgressBar progressBar;
        private TextView approvedText;

        TrainingListAdapterHolder(@NonNull View itemView) {
            super(itemView);

            titleText = itemView.findViewById(R.id.title_training_text);
            usernameText = itemView.findViewById(R.id.username_text_video);
            imageView = itemView.findViewById(R.id.video_view);
            ratingBar = itemView.findViewById(R.id.rating_bar_video);
            progressBar = itemView.findViewById(R.id.image_progress_bar);

            if (user.getRole().equals(Role.COACH))
                approvedText = itemView.findViewById(R.id.approved_text);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ViewTrainingActivity.class);
                    intent.putExtra("videoId", training.getEntityKey());
                    context.startActivity(intent);
                }
            });
        }

        private void setDetails(PublicatedTraining publicatedTraining) {
            training = publicatedTraining;
            titleText.setText(publicatedTraining.getTitle());
            if (user.getRole().equals(Role.COACH)) {
                StringBuilder approveText = new StringBuilder();
                if (!publicatedTraining.isApproved()) {
                    approveText.append("Entraînement non approuvé");
                    this.approvedText.setTextColor(Color.RED);
                }
                else {
                    approveText.append("Entraînement approuvé");
                    this.approvedText.setTextColor(Color.GREEN);
                }
                this.approvedText.setText(approveText.toString());
            }
            usernameText.setText(publicatedTraining.getPublicatorUsername());
            if (publicatedTraining.getStars() == -1)
                ratingBar.setNumStars(0);
            else
                ratingBar.setRating(publicatedTraining.getStars());
            // imageView.setVideoURI(publicatedTraining.getVideoUri());
            // imageView.seekTo(1);
            if (publicatedTraining.getVideoFile() != null) {
                imageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(publicatedTraining.getVideoFile().getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND));
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
