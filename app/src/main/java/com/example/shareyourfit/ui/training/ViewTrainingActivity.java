package com.example.shareyourfit.ui.training;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.shareyourfit.R;
import com.example.shareyourfit.data.model.training.Mark;
import com.example.shareyourfit.data.model.training.PublicatedTraining;
import com.example.shareyourfit.data.model.user.Role;
import com.example.shareyourfit.data.model.user.User;
import com.example.shareyourfit.data.repository.FirestoreRepository;
import com.example.shareyourfit.ui.MainActivity;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class ViewTrainingActivity extends AppCompatActivity {

    private PlayerView playerView;
    private SimpleExoPlayer player;


    private boolean playWhenReady = true;
    private int currentWindow = 0;
    private long playbackPosition = 0;

    private User CurrentUser;
    private PublicatedTraining currentTraining;
    private StorageReference storageReference;
    private FirestoreRepository<PublicatedTraining> publicatedTrainingRepository;
    private FirestoreRepository<Mark> marksFirestoreRepository;
    private FirestoreRepository<User> userRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_training);

        final ProgressDialog dialog = ProgressDialog.show(this, "Chargement", "Chargement...", true);

        Intent intent = getIntent();
        String videoId = intent.getStringExtra("videoId");
        publicatedTrainingRepository = new FirestoreRepository<>(PublicatedTraining.class, "publicated_trainings");
        marksFirestoreRepository = new FirestoreRepository<>(Mark.class, "marks");
        userRepo = new FirestoreRepository<>(User.class, MainActivity.USER_REPO);


        publicatedTrainingRepository.get(videoId).addOnCompleteListener(new OnCompleteListener<PublicatedTraining>() {
            @Override
            public void onComplete(@NonNull Task<PublicatedTraining> task) {
                if (task.isSuccessful()) {
                    currentTraining = Objects.requireNonNull(task.getResult());

                    storageReference = FirebaseStorage.getInstance().getReference();
                    final File localFile = createTempFile(currentTraining.getEntityKey(), ".mp4");
                    storageReference.child(currentTraining.getEntityKey()).getFile(localFile).addOnCompleteListener(new OnCompleteListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<FileDownloadTask.TaskSnapshot> task) {
                            if (task.isSuccessful()) {
                                currentTraining.setVideoFile(localFile);
                                currentTraining.setVideoUri(Uri.fromFile(localFile));
                                initializePlayer();
                            } else {
                                Objects.requireNonNull(task.getException()).printStackTrace();
                            }

                            dialog.dismiss();
                        }
                    });

                    initUi();

                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });




        // Get the video View
        playerView = findViewById(R.id.video_view);

    }

    private void initUi() {
        Task<User> userTask = userRepo.get(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).addOnCompleteListener(new OnCompleteListener<User>() {
            @Override
            public void onComplete(@NonNull Task<User> task) {
                if (task.isSuccessful()) {
                    CurrentUser = task.getResult();

                    if(CurrentUser.getRole().getRole().equals(Role.COACH.getRole()))
                    {
                        Button approveButton = findViewById(R.id.approveButton);

                        if(!currentTraining.isApproved())
                        {
                            Toast.makeText(getBaseContext(), "training pas approuvé", Toast.LENGTH_SHORT).show();
                            approveButton.setVisibility(View.VISIBLE);
                            approveButton.setEnabled(true);
                            approveButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ApproveCurrentTraining();
                                }
                            });
                        }
                    }
                } else {

                    Objects.requireNonNull(task.getException()).printStackTrace();
                }
            }
        });

        TextView videoName = findViewById(R.id.video_name);
        videoName.setText(currentTraining.getTitle());

        TextView userName = findViewById(R.id.username);
        userName.setText(currentTraining.getPublicatorUsername());

        TextView description = findViewById(R.id.descriptionText);
        description.setText(currentTraining.getDescription());

        TextView tags = findViewById(R.id.tag_list);
        StringBuilder tagDisplay = new StringBuilder();
        for (String tag :
                currentTraining.getTags()) {
            tagDisplay.append(tag).append(" ");
        }

        tags.setText(tagDisplay.toString());

        TextView nbRatings = findViewById(R.id.nb_votes);
        nbRatings.setText("(" + currentTraining.getMarks().size() + ")");

        RatingBar ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setRating(currentTraining.getStars());
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (fromUser) {
                    boolean hasVoted = false;
                    Mark currentMark = null;
                    for (Mark mark : currentTraining.getMarks()) {
                        if (mark.getUserID().equals(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())) {
                            hasVoted = true;
                            currentMark = mark;
                        }
                    }

                    if (hasVoted) {
                        int previousRating = currentMark.getNbStars();
                        currentMark.setNbStars(Math.round(rating));
                        UpdateMark(currentMark, previousRating);
                    } else
                        AddMark(rating);
                }
            }
        });
    }
    private void ApproveCurrentTraining()
    {
        currentTraining.setApproved(true);
        publicatedTrainingRepository.update(currentTraining).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Button approveButton = findViewById(R.id.approveButton);
                    approveButton.setVisibility(View.INVISIBLE);
                    approveButton.setEnabled(false);
                    Toast.makeText(getBaseContext(), "Training approve", Toast.LENGTH_SHORT).show();
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                    Toast.makeText(getBaseContext(), "The Training cannot be approve, please try later.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void UpdateMark(final Mark currentMark, final int previousRating) {
        currentTraining.UpdateMark(currentMark);

        publicatedTrainingRepository.update(currentTraining).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    RatingBar ratingBar = findViewById(R.id.ratingBar);
                    ratingBar.setRating(currentTraining.getStars());

                    TextView nbRatings = findViewById(R.id.nb_votes);
                    nbRatings.setText("(" + currentTraining.getMarks().size() + ")");

                    Toast.makeText(getBaseContext(), "Vote updated", Toast.LENGTH_SHORT).show();
                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                    currentMark.setNbStars(previousRating);
                    currentTraining.UpdateMark(currentMark);
                    Toast.makeText(getBaseContext(), "Your vote cannot be saved, please try later.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void AddMark(float rating) {
        final Mark newMark = new Mark(Math.round(rating), Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        final DocumentReference reference = marksFirestoreRepository.getCollectionReference().document();

        newMark.setEntityKey(reference.getId());

        currentTraining.addMark(newMark);

        publicatedTrainingRepository.update(currentTraining).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    RatingBar ratingBar = findViewById(R.id.ratingBar);
                    ratingBar.setRating(currentTraining.getStars());

                    TextView nbRatings = findViewById(R.id.nb_votes);
                    nbRatings.setText("(" + currentTraining.getMarks().size() + ")");

                    Toast.makeText(getBaseContext(), "Vote added", Toast.LENGTH_SHORT).show();

                } else {
                    Objects.requireNonNull(task.getException()).printStackTrace();
                    Toast.makeText(getBaseContext(), "Your vote cannot be saved, please try later.", Toast.LENGTH_SHORT).show();

                    // delete the mark as it's not linked to a training
                    currentTraining.removeMark(newMark);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUi();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Util.SDK_INT < 24) {
            releasePlayer();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Util.SDK_INT >= 24) {
            releasePlayer();
        }
    }

    private void initializePlayer() {
        player = new SimpleExoPlayer.Builder(this).build();
        playerView.setPlayer(player);
        Uri uri = currentTraining != null ? currentTraining.getVideoUri() : null;
        MediaSource mediaSource = buildMediaSource(uri);
        player.setPlayWhenReady(playWhenReady);
        player.seekTo(currentWindow, playbackPosition);
        player.prepare(mediaSource, false, false);
    }

    private MediaSource buildMediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, "exoplayer-codelab");
        return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void releasePlayer() {
        if (player != null) {
            playWhenReady = player.getPlayWhenReady();
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            player.release();
            player = null;
        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static File createTempFile(String prefix, String suffix) {
        File parent = new File(Objects.requireNonNull(System.getProperty("java.io.tmpdir")));

        File temp = new File(parent, prefix + suffix);

        if (temp.exists()) {
            temp.delete();
        }

        try {
            temp.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return temp;
    }


    public void finishActivity(View v) {
        finish();
    }
}
